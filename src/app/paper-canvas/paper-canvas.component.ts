import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PaperScope, Project } from 'paper';

@Component({
  selector: 'app-paper-canvas',
  templateUrl: './paper-canvas.component.html',
  styleUrls: ['./paper-canvas.component.scss']
})
export class PaperCanvasComponent implements OnInit {
  @ViewChild('canvasElement') canvasElement: ElementRef;
  @ViewChild('baseImage') imageElement: ElementRef;
  scope: PaperScope;
  project: Project;

  constructor() { }

  ngOnInit() {

  }

  updateCanvasSize() {
    const imgWidth = this.imageElement.nativeElement.width;
    this.canvasElement.nativeElement.width = imgWidth;
    this.canvasElement.nativeElement.height = this.imageElement.nativeElement.height;
    this.canvasElement.nativeElement.style = `transform: translate(${-imgWidth}px, 0)`;
  }
}
