import { Component, ViewChild, ElementRef } from '@angular/core';
import { PaperCanvasComponent } from './paper-canvas/paper-canvas.component';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'eyedentify';
  initializeURL = '/api/initialize';
  identifyURL = '/api/identify';
  @ViewChild('logo') imageElement: ElementRef;
  @ViewChild(PaperCanvasComponent)
  private paperCanvasComponent: PaperCanvasComponent;
  identifiedName: string;

  constructor(private http: HttpClient) {
    // http.post(this.initializeURL, {}).subscribe(() => console.log('server initialized'));
  }

  previewURL(event: any): void {
    // const preview = document.querySelector('img');
    const preview = this.paperCanvasComponent.imageElement;
    const url = event.target.value;
    preview.nativeElement.src = url;
    this.paperCanvasComponent.updateCanvasSize();
    this.identifiedName = '';
    if (url !== '') {
      this.http.post(this.identifyURL, { image_url: url }).subscribe((data: any) => {
        console.log(data);
        if (data && data.name) {
          this.identifiedName = data.name;
        }
      });
    }
  }

  // previewFile(event: any): void {
  //   const preview = document.querySelector('img'); // selects the query named img
  //   // const file = (document.querySelector('input[type=file]') as any).files[0]; // sames as here
  //   const file = event.target.files[0];
  //   const reader = new FileReader();

  //   reader.onloadend = () => {
  //     preview.src = reader.result as string;
  //     if (reader.result !== '') {
  //       this.paperCanvasComponent.updateCanvasSize();
  //       // https://www.eaiti.com/jump.c3d3b85cb362f58bc345.jpg
  //       this.http.post(this.identifyURL, { image_url: 'https://i.imgur.com/ZwYzSqL.jpg' }).subscribe((data: any) => {
  //         console.log(data);
  //         if (data && data.name) {
  //           this.identifiedName = data.name;
  //         }
  //       });
  //     }
  //   };

  //   if (file) {
  //     reader.readAsDataURL(file); // reads the data as a URL
  //   } else {
  //     preview.src = '';
  //   }
  // }

  drawName(name: string, x: number, y: number): void {

  }
}
